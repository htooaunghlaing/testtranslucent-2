package com.example.guidovezzoni.testtranslucent2;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;


public class MainActivity extends Activity {
    private Button mStartNewActivity;
    private LinearLayout mMainLayout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        mMainLayout = (LinearLayout) findViewById(R.id.mainll);

        mStartNewActivity = (Button) findViewById(R.id.button);
        mStartNewActivity.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(MainActivity.this, OtherActivity.class));
//                Handler h = new Handler();
//                h.postDelayed(new Runnable() {
//                    @Override
//                    public void run() {
//                        mMainLayout.setBackgroundResource(R.color.colorPrimary);
//                    }
//                }, 2000);

            }
        });
        mMainLayout.setBackgroundColor(Color.GREEN);
//        setTheme(android.R.style.Theme_Holo);
    }

}
