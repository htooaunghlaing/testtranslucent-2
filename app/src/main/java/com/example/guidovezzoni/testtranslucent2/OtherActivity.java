package com.example.guidovezzoni.testtranslucent2;

import android.app.Activity;
import android.os.Bundle;

/**
 * Created by guidovezzoni on 18/11/15.
 */
public class OtherActivity extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_other);

    }
}
